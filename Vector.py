#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of PHYMOBAT 1.2.
# Copyright 2016 Sylvio Laventure (IRSTEA - UMR TETIS)
#
# PHYMOBAT 1.2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PHYMOBAT 1.2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PHYMOBAT 1.2.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import subprocess
import numpy as np
from osgeo import ogr
import otbApplication as otb
from collections import *

import Outils
from RasterSat_by_date import RasterSat_by_date

class Vector():
	"""
		Vector class to extract a area, vector data and zonal statistic

		@param vector_used: Input/Output shapefile to clip (path)
		@type vector_used: str

		@param vector_cut: Area shapefile (path)
		@type vector_cut: str

		@param vector_name: Name of the shapefile
		@type vector_name: str

		@param vector_folder: Name of the folder containing the shapefile
		@type vector_name: str

		@param data_source: Input shapefile information
		@type data_source: ogr pointer

		@param stats_dict: Stats results
		@type stats_dict: dict

		@param remove_shp: Remove shapefile or not. 0 : don't remove, 1 : remove
		@type remove_shp: int

		@opt: **Remove** (int) - For the remove_shp variable
	"""

	def __init__(self, used, cut, **opt):
		"""
			Create a new 'Vector' instance
		"""

		if not hasattr(self, "logger") :
			self.logger = Outils.Log("Log", 'vector')

		self.vector_name = os.path.basename(used)
		self.vector_folder = os.path.dirname(used)

		self.vector_cut = cut
		self.vector_used = used
		self.remove_shp = opt['Remove'] if opt.get('Remove') else 0

		self.stats_dict = defaultdict(list)

	def clip_vector(self, output_folder):
		"""
			Function to clip a vector with a vector
		"""

		if not os.path.exists(output_folder) :
			os.makedirs(output_folder)

		outclip = "{0}/Clip_{1}".format(output_folder, self.vector_name)

		if not os.path.exists(outclip) or self.remove_shp == 1:
			self.logger.info('Clip of {0}'.format(self.vector_name))

			# Command to clip a vector with a shapefile by OGR
			process_tocall_clip =\
			 ['ogr2ogr', '-overwrite',\
			  '-skipfailures', outclip, self.vector_used, \
			  '-clipsrc', self.vector_cut]

			subprocess.call(process_tocall_clip)

		# Replace input filename by output filename
		self.vector_used = outclip

	def vector_data(self):
		"""
			Function to extract vector layer information
		"""

		try:
			self.data_source = ogr.Open(self.vector_used)
			self.logger.info('Shapefile opening : {0}'.format(self.data_source.GetLayer().GetName()))
		except :
			self.logger.error('Could not open file')
			sys.exit(1)

		# List of field name
		self.field_names = [self.data_source.GetLayer().GetLayerDefn().GetFieldDefn(l).GetName() \
						for l in range(self.data_source.GetLayer().GetLayerDefn().GetFieldCount())]

	def close_data(self):
		"""
			Function to remove allocate memory
		"""

		self.logger.info('Shapefile closing : {0}'.format(self.data_source.GetLayer().GetName()))

		# Close data sources
		self.data_source.Destroy()

	def zonal_stats(self, liste_chemin):
		"""
			Function to compute the mean in every polygons on a list images with otb

			@param liste_chemin : List input image path
			@type  liste_chemin : list(string)
		"""

		self.logger.info("Compute stats 'mean' on {0}".format(os.path.split(self.vector_used)[1]))

		zonal_stats = otb.Registry.CreateApplication("ZonalStatistics")
		zonal_stats.SetParameterStringList("il", liste_chemin)
		zonal_stats.SetParameterStringList("vec", [self.vector_used])
		zonal_stats.SetParameterStringList("stats", ["mean"])

		zonal_stats.Execute()

		stats_otb = list(zonal_stats.GetParameterValue("out"))

		liste_valeur = []

		for stats in stats_otb :
			for s in stats.split(";"):
				if s :
					liste_valeur.append(np.array([float(x) for x in s.split()]))

		resultats = np.dstack(liste_valeur)[0]

		stats_dict = defaultdict(list)

		for idx, r in enumerate(resultats) :
			self.stats_dict[idx] = r.tolist()

		self.logger.info("End of stats 'mean' on {0}".format(os.path.split(self.vector_used)[1]))

	def zonal_stats_pp(self, inraster):
		"""
			A zonal statistics ++ to dertermine pxl percent in every polygon

			:param inraster: Input image path
			:type inraster: str

			:returns: dict -- **p_stats** : dictionnary with pxl percent in every polygon. Mainly 'Maj_count' (Majority Value) and 'Maj_count_perc' (Majority Percent)
		"""

		zonal_stats = otb.Registry.CreateApplication("ZonalStatistics")
		zonal_stats.SetParameterStringList("il", [inraster])
		zonal_stats.SetParameterStringList("vec", [self.vector_used])
		zonal_stats.SetParameterStringList("stats", ["count"])

		zonal_stats.Execute()

		stats_otb = list(zonal_stats.GetParameterValue("out"))

		p_stats = []

		for item in stats_otb:
			liste_item = item.split()
			dico = dict()

			dico['count'] = float(liste_item[0])
			dico['Maj_count'] = float(liste_item[1])
			dico['Maj_count_perc'] = float(liste_item[2])
			p_stats.append(dico)

		return p_stats

	def layer_rasterization(self, raster_head, attribute_r, **kwargs):
		"""
			Function to rasterize a vector using OTB.

			@param raster_head: Raster path that will look like the final raster of the rasterization
			@type raster_head: str

			@param attribute_r: Field name of the shapefile that contains class names
			@type attribute_r: str

			@kwargs: **choice_nb_b** (int) - Output image number of band. If you choice 1, take first band. If you choice 2, take two first band etc...
			@returns: str -- **valid_raster** : output raster path from the rasterization
		"""

		valid_raster = "{0}.TIF".format(self.vector_used[:-4])# Name of the output raster

		self.logger.debug("valid_raster : {0}".format(valid_raster))

		if os.path.exists(valid_raster):
			os.remove(valid_raster)

		layerRasterization = otb.Registry.CreateApplication("Rasterization")
		layerRasterization.SetParameterString("in", self.vector_used)
		layerRasterization.SetParameterString("out", valid_raster)
		layerRasterization.SetParameterString("im", raster_head)
		layerRasterization.SetParameterString("mode", "attribute")
		layerRasterization.SetParameterString("mode.attribute.field", attribute_r)

		layerRasterization.ExecuteAndWriteOutput()

		return valid_raster
