#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, time
from PyQt5 import QtWidgets, QtCore

from ui_A_propos_PHYMOBAT_window import Ui_About
from ui_Warming_study_area import Ui_Warming_study_area
from ui_Warming_forgetting import Ui_Warming_forgetting
from ui_Proxy_window import Ui_Proxy_window

class about(QtWidgets.QWidget):
	"""
		Popup to display "About PHYMOBAT". In this windows, it prints informations on the processing, license
		and version.
	"""
	def __init__(self, parent=None):
		QtWidgets.QWidget.__init__(self, parent)
		self.prop = Ui_About()
		self.prop.setupUi(self)
		
		self.prop.close_newWindow.clicked.connect(self.close_window)
	
	def close_window(self):
		"""
			Function to close the "A propos PHYMOBAT".
		"""
		self.close()
		
class warming_study_area(QtWidgets.QWidget):
	"""
		Popup to display a message to say there isn't declared study area file.
	"""
	def __init__(self, parent=None):
		QtWidgets.QWidget.__init__(self, parent)
		self.w_study_a = Ui_Warming_study_area()
		self.w_study_a.setupUi(self)
		
		self.w_study_a.pushButton_ok_window_warning_study_area.clicked.connect(self.close_window)
	
	def close_window(self):
		"""
			Function to close the popup.
		"""
		self.close() 
		
class warming_forgetting(QtWidgets.QWidget):
	"""
		Popup to display a message to tell you if you fogotten to enter a raster or a sample.
	"""
	def __init__(self, parent=None):
		QtWidgets.QWidget.__init__(self, parent)
		self.w_forget = Ui_Warming_forgetting()
		self.w_forget.setupUi(self)
		
		self.w_forget.pushButton_ok_forget.clicked.connect(self.close_window)
	
	def close_window(self):
		"""
			Function to close the popup.
		"""
		self.close() 
		
class proxy_window(QtWidgets.QWidget):
	"""
		Popup to display a message to tell you if you fogotten to enter a raster or a sample.
	"""
	def __init__(self, parent=None):
		QtWidgets.QWidget.__init__(self, parent)
		self.w_proxy = Ui_Proxy_window()
		self.w_proxy.setupUi(self)
		
		# Proxy ID
		self.proxy = ""
		self.login_proxy = ""
		self.password_proxy = ""
		
		# Connect Apply|Close button
		self.w_proxy.buttonBox_proxy.button(QtWidgets.QDialogButtonBox.Close).clicked.connect(self.close_window)
		self.w_proxy.buttonBox_proxy.button(QtWidgets.QDialogButtonBox.Apply).clicked.connect(self.id_proxy)
	
	def id_proxy(self):
		"""
			Function to use input proxy id
		"""
		self.login_proxy = "{0}".format(self.w_proxy.lineEdit_login_proxy.text())
		self.password_proxy = "{0}".format(self.w_proxy.lineEdit_password_proxy.text())
		self.proxy = "{0}".format(self.w_proxy.lineEdit_proxy.text())

		self.close_window()

	def close_window(self):
		"""
			Function to close the popup.
		"""
		self.close()
