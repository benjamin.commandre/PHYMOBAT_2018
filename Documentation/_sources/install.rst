Installation
============

Téléchargement Phymobat
----------------------------

Le programme Phymobat peut être cloné à partir du Gitlab Irstea à l'adresse suivante : https://gitlab.irstea.fr/benjamin.commandre/PHYMOBAT_2018.git

La branche courante est ``develop``

Installation SIG open source
----------------------------

La chaîne de traitement est construite sous divers outils open-source, comme ``GDAL`` et ``OGR``. La démarche à suivre pour installer ces outils est indiquée ci-dessous uniquement sous Linux.

- Ajouter le dépôt ubuntugis-unstable

.. code-block:: bash
	
	$ sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
	$ sudo apt-get update

GDAL
~~~~~~~~~~~~~~

.. code-block:: bash
	
	$ sudo apt-get install gdal-bin

Pour vérifier que GDAL est bien installé, taper :

.. code-block:: bash

	$ gdalinfo

Il est bien installé, si vous avez l'aide de gdalinfo qui s'affiche (Idem pour OGR) :

.. code-block:: bash

	Usage: gdalinfo [--help-general] [-mm] [-stats] [-hist] [-nogcp] [-nomd]
                [-norat] [-noct] [-nofl] [-checksum] [-proj4]
                [-listmdd] [-mdd domain|`all`]*
                [-sd subdataset] datasetname

	FAILURE: No datasource specified.

Orfeo ToolBox (OTB)
~~~~~~~~~~~~~~~~~~~~~~~~~~

Commencer par installer les dépendances requises avec la commande suivante :

.. code-block:: bash

	$ sudo aptitude install make cmake-curses-gui build-essential libtool automake git libbz2-dev python-dev libboost-dev libboost-filesystem-dev libboost-serialization-dev libboost-system-dev zlib1g-dev libcurl4-gnutls-dev swig 

Cloner la dernière version d'OTB présente sur le git à l'url : https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb, par exemple :

.. code-block:: bash

	$ git clone https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb.git

Suivez les instructions du manuel d'utilisation d'OTB, https://www.orfeo-toolbox.org/packages/OTBSoftwareGuide.pdf, pour compiler en mode "SuperBuild".

Cloner le module SimpleExtractionTools à l'url: https://gitlab.irstea.fr/benjamin.commandre/SimpleExtractionToolsPhymobat dans le répertoire ``OTB/Module/Remote`` puis compilez et installez-le.

CURL
~~~~~~~~~~~~~~~~~~

Vérifier que le package CURL est installé, sur certaines versions de Ubuntu il ne l'est pas :

.. code-block:: bash

	$ apt-cache search curl

	i A curl                                           - outil en ligne de commande pour transférer des données avec une syntaxe URL
	p   curl:i386                                      - outil en ligne de commande pour transférer des données avec une syntaxe URL                
	p   curlftpfs                                      - Système de fichiers pour accéder à des hôtes FTP, basé sur FUSE et cURL 

S'il ne l'est pas : 

.. code-block:: bash 

	$ sudo apt-get install curl
   

Installation des modules python
--------------------------------------------

La version de Python utilisée est la 3.6.5

Installer les deux modules Python ``gdal``, ``scikit-learn``, ``Shapely``, ``numpy``, ``lxml`` et ``pyQt5`` depuis le dépôt du système de la façon suivante :

.. code-block:: bash

	$ sudo aptitude install python­3-gdal python­3-sklearn python3-shapely python3-numpy python3-lxml libqt5 pyqt5-dev-tools 

Pour vérifier si les modules sont bien installé ou dèjà installé, il suffit de taper dans la console Python (Par exemple pour le module gdal):

>>> from osgeo import gdal
>>> 

Si ils ne sont pas encore installé vous aurez ces réponses. Dans ce cas il faudra les installer (voir section ci-dessus) :

>>> from osgeo import gdal
ImportError: cannot import name gdal

Ou

>>> from osgeo import gdal
ImportError: No module named gdal
