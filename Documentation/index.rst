.. PHYMOBAT documentation master file, created by
   sphinx-quickstart on Mon Jan  4 17:26:09 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation de PHYMOBAT
=========================

Chaîne de traintement du Fond blanc PHYsionomique des Milieux Ouverts de Basse Altitude par Télédétection (PHYMOBAT).

.. note:: Outil développé sur Linux-Ubuntu 18.04.

.. toctree::
  :maxdepth: 3

  _sources/install.rst
  _sources/methode_tuto.rst
  _sources/package.rst
  _sources/API.rst
