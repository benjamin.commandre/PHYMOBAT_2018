\contentsline {chapter}{\numberline {1}Installation}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}T\IeC {\'e}l\IeC {\'e}chargement Phymobat}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Installation SIG open source}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}GDAL}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Orfeo ToolBox (OTB)}{4}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}CURL}{4}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Installation des modules python}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}Processus utilis\IeC {\'e}es et tutoriels API}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Processus algorithmiques utilis\IeC {\'e}es}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Traitement des images}{9}{subsection.2.1.1}
\contentsline {subsubsection}{1. Listing et t\IeC {\'e}l\IeC {\'e}chargements des images sur la plate-forme Theia}{10}{subsubsection*.3}
\contentsline {subsubsection}{2. Traitements des images t\IeC {\'e}l\IeC {\'e}charg\IeC {\'e}es}{11}{subsubsection*.4}
\contentsline {subsubsection}{3. Traitements des images THRS}{12}{subsubsection*.5}
\contentsline {subsection}{\numberline {2.1.2}Traitements des \IeC {\'e}chantillons}{14}{subsection.2.1.2}
\contentsline {subsubsection}{1. Mod\IeC {\`e}le Seath}{14}{subsubsection*.6}
\contentsline {subsubsection}{2. Mold\IeC {\`e}le Random Forest (RF)}{14}{subsubsection*.7}
\contentsline {subsection}{\numberline {2.1.3}Traitements de classification}{15}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Tutoriels interface}{16}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Interface Simplifi\IeC {\'e}e}{16}{subsection.2.2.1}
\contentsline {subsubsection}{Exemple sur un jeu de donn\IeC {\'e}es test}{18}{subsubsection*.8}
\contentsline {subsection}{\numberline {2.2.2}Interface experte}{19}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Interface du traitement des images}{19}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Interface du traitement des \IeC {\'e}chantillons}{23}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Interface du traitement de classification}{25}{subsection.2.2.5}
\contentsline {chapter}{\numberline {3}CarHab Phy MOBA package}{35}{chapter.3}
\contentsline {section}{\numberline {3.1}Image processing}{35}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Archive}{35}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Landsat image processing}{37}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Texture index processing}{38}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Slope processing}{39}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Toolbox}{39}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Vector processing}{40}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Super vector}{40}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Sample}{41}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}RPG}{42}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Separability and threshold index}{42}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Classification}{43}{subsection.3.2.5}
\contentsline {chapter}{\numberline {4}Graphical User Interface package}{45}{chapter.4}
\contentsline {section}{\numberline {4.1}Interface command}{46}{section.4.1}
\contentsline {section}{\numberline {4.2}Control processing}{49}{section.4.2}
\contentsline {section}{\numberline {4.3}Interface element}{52}{section.4.3}
\contentsline {section}{\numberline {4.4}Popup about and warming}{52}{section.4.4}
\contentsline {chapter}{Index des modules Python}{53}{section*.113}
\contentsline {chapter}{Index}{55}{section*.114}
