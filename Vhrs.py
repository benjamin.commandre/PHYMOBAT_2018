#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of PHYMOBAT 1.2.
# Copyright 2016 Sylvio Laventure (IRSTEA - UMR TETIS)
# 
# PHYMOBAT 1.2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PHYMOBAT 1.2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PHYMOBAT 1.2.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import glob
import Outils
import otbApplication as otb

from multiprocessing import Process

class Vhrs():
	"""
		Class to compute Haralick and SFS textures with OTB
		
		@param imag: The input image path to compute texture image
		@type imag: str

		@param out_sfs/out_haralick: Output path
		@type out_sfs/out_haralick: str

		@param mp: Boolean variable -> 0 or 1.
			- 0 means, not multi-processing
			- 1 means, launch process with multi-processing
		@type mp: int
	"""
	
	def __init__(self, imag, mp):
		"""
			Create a new 'Texture' instance
		"""
		
		self._image = imag
		self.mp = mp
		self.logger = Outils.Log('Log', "VHRS")

		self.out_sfs 	   = "{0}_sfs.TIF".format(self._image[:-4])
		self.out_haralick  = "{0}_haralick.TIF".format(self._image[:-4])
		
		p_sfs = Process(target=self.sfs_texture_extraction)
		p_sfs.start()

		if mp == 0:
			p_sfs.join()

		p_har = Process(target=self.haralick_texture_extraction, args=('simple', ))
		p_har.start()
			
		if mp == 0:
			p_har.join()
		
		if mp == 1:
			p_sfs.join()
			p_har.join()
		
	def sfs_texture_extraction(self):
		"""
			Function to compute OTB SFS texture image				
				- OTB help :
					* in : Input Image
					* channel : Selected Channel
					* parameters : Texture feature parameters. This group of parameters allows to define SFS texture parameters. The available texture features are SFS’Length, SFS’Width, SFS’PSI, SFS’W-Mean, SFS’Ratio and SFS’SD. They are provided in this exact order in the output image.
						- parameters.spethre : Spectral Threshold
						- parameters.spathre : Spatial Threshold
						- parameters.nbdir : Number of Direction
						- parameters.alpha : Alpha
						- parameters.maxcons : Ratio Maximum Consideration Number
					* out : Feature Output Image
		"""

		self.logger.info('SFS image')

		if not os.path.exists(self.out_sfs):

			self.logger.info("SFS image doesn't exist !")
			  
			otb_SFS = otb.Registry.CreateApplication("SFSTextureExtraction")
			otb_SFS.SetParameterString("in", self._image)
			otb_SFS.SetParameterInt("channel", 2)
			otb_SFS.SetParameterInt("parameters.spethre", 50)
			otb_SFS.SetParameterInt("parameters.spathre", 100)
			otb_SFS.SetParameterString("out", self.out_sfs)

			otb_SFS.ExecuteAndWriteOutput()

			self.logger.info('SFS image created.')		

	def haralick_texture_extraction(self, texture_choice):
		"""
			Function to compute OTB Haralick texture image
				- OTB help :
					* in : Input Image
					* channel : Selected Channel
					* Texture feature parameters : This group of parameters allows to define texture parameters.
						- X Radius : X Radius
						- Y Radius : Y Radius
						- X Offset : X Offset
						- Y Offset : Y Offset
					* Image Minimum : Image Minimum
					* Image Maximum : Image Maximum
					* Histogram number of bin : Histogram number of bin 
					* Texture Set Selection Choice of The Texture Set Available choices are :
						- Simple Haralick Texture Features: This group of parameters defines the 8 local Haralick texture feature output image. The image channels are: Energy, Entropy, Correlation, Inverse Difference Moment, Inertia, Cluster Shade, Cluster Prominence and Haralick Correlation
						- Advanced Texture Features: This group of parameters defines the 9 advanced texture feature output image. The image channels are: Mean, Variance, Sum Average, Sum Variance, Sum Entropy, Difference of Entropies, Difference of Variances, IC1 and IC2
						- Higher Order Texture Features: This group of parameters defines the 11 higher order texture feature output image. The image channels are: Short Run Emphasis, Long Run Emphasis, Grey-Level Nonuniformity, Run Length Nonuniformity, Run Percentage, Low Grey-Level Run Emphasis, High Grey-Level Run Emphasis, Short Run Low Grey-Level Emphasis, Short Run High Grey-Level Emphasis, Long Run Low Grey-Level Emphasis and Long Run High Grey-Level Emphasis
					* out : Feature Output Image 
					
				Source : http://otbcb.readthedocs.org/en/latest/Applications/app_HaralickTextureExtraction.html
			
			@param texture_choice: Order texture choice -> Simple / Advanced / Higher
			@type texture_choice: str
		"""
		self.logger.info('Haralick image')

		if not os.path.exists(self.out_haralick):

			self.logger.info("Haralick image doesn't exist !")

			otb_Haralick = otb.Registry.CreateApplication("HaralickTextureExtraction")
			otb_Haralick.SetParameterString("in", self._image)
			otb_Haralick.SetParameterInt("channel", 2)
			otb_Haralick.SetParameterInt("parameters.xrad", 3)
			otb_Haralick.SetParameterInt("parameters.yrad", 3)
			otb_Haralick.SetParameterString("texture", texture_choice)
			otb_Haralick.SetParameterString("out", self.out_haralick)

			otb_Haralick.ExecuteAndWriteOutput()

			self.logger.info('Haralick image created.')
