#!/bin/env python
# -*- coding: utf-8 -*-

import argparse
import logging
from logging.handlers import RotatingFileHandler
import signal
from contextlib import contextmanager
import inspect
import traceback

try:
	import Constantes
except :
	pass


class Fusion(argparse.Action):
	def __init__(self, option_strings, dest, nargs=None, **kwargs):
		super(Fusion, self).__init__(option_strings, dest, nargs, **kwargs)
	def __call__(self, parser, namespace, values, option_string=None):
		setattr(namespace, self.dest, ' '.join(values).lower())

class Log(object):

	def __init__(self, dossier, nomFichier, niveau=None):

		super(Log, self).__init__()

		stack = inspect.stack()
		nom_classe = stack[1][0].f_locals["self"].__class__.__name__

		self.__logger__ = logging.getLogger(nomFichier)

		if niveau is None:
			try:
				niveau = Constantes.NIVEAU_DEFAUT
			except :
				niveau = logging.DEBUG

		self.__logger__.setLevel(niveau)

		format_fichier = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
		fichierLog = RotatingFileHandler("{0}/{1}.log".format(dossier, nomFichier), 'a', 1000000, 1)

		fichierLog.setLevel(niveau)
		fichierLog.setFormatter(format_fichier)
		self.__logger__.addHandler(fichierLog)

		format_console = logging.Formatter('%(asctime)s :: {0} :: %(levelname)s :: %(message)s'.format(nom_classe), datefmt='%d-%m-%Y %H:%M:%S')

		console = logging.StreamHandler()
		console.setLevel(niveau)
		console.setFormatter(format_console)
		self.__logger__.addHandler(console)

	def info(self, message):
		self.__logger__.info(message)

	def debug(self, message):
		self.__logger__.debug(message)

	def warning(self, message):
		self.__logger__.warning(message)

	def error(self, message):
		self.__logger__.error(message)

	def critical(self, message):
		self.__logger__.critical(message)

	def exception(self, message):
		self.__logger__.exception(message)

	def close(self):
		for handler in  self.__logger__.handlers[:] :
			handler.close()
			self.__logger__.removeHandler(handler)


class TimeoutException(Exception): pass

@contextmanager
def limitation_temporelle(secondes):
    
    def signal_handler(signum, frame):
        raise TimeoutException

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(secondes)

    try:
        yield
    finally:
        signal.alarm(0)

# Utilisation :
#
# try:
#     with limitation_temporelle(temps_en_seconde):
#         appel_fonction()
# except TimeoutException:
#     pass