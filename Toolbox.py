#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of PHYMOBAT 1.2.
# Copyright 2016 Sylvio Laventure (IRSTEA - UMR TETIS)
# 
# PHYMOBAT 1.2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PHYMOBAT 1.2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PHYMOBAT 1.2.  If not, see <http://www.gnu.org/licenses/>.

from datetime import date

import otbApplication as otb

import os, sys, subprocess, glob
import numpy as np
import json
import osgeo.gdal
import Constantes
import Outils

class Toolbox(object):
	"""
		Class used to grouped small tools to cut raster or compute statistics on a raster. 
			
		:param imag: Input image (path)
		:type imag: str
		
		:param vect: Extent shapefile (path)
		:type vect: str
	"""
	
	def __init__(self):
		"""
			Create a new 'Toolbox' instance
		"""
		self.image = None
		self.vect = None
		self.output = None
		self.logger = Outils.Log("Log", "Toolbox")
	
	def clip_raster(self, **kwargs):
		"""
			Function to clip a raster with a vector. 
			The raster created will be in the same folder than the input raster.
			With a prefix 'Clip_'.
			
			:kwargs: **rm_rast** (int) - 0 (by default) or 1. Variable to remove the output raster. 0 to keep and 1 to remove.
			
			:returns: str -- variable **outclip**, output raster clip (path).
		"""
		
		rm_rast = kwargs['rm_rast'] if kwargs.get('rm_rast') else 0

		if self.output is None :
			outclip = "{0}/Clip_{1}".format(os.path.dirname(self.image), os.path.basename(self.image))
		else :
			if not os.path.exists(os.path.dirname(self.output)) :
				os.mkdir(os.path.dirname(self.output))

			outclip = self.output

		options = osgeo.gdal.WarpOptions(dstNodata=-10000, cropToCutline=True,\
		cutlineDSName=self.vect,outputType=osgeo.gdal.GDT_Int16 , format='GTiff')

		if not os.path.exists(outclip) or rm_rast == 1:
			self.logger.info("Raster clip of {0}".format(os.path.basename(self.image)))
			osgeo.gdal.Warp(outclip, self.image, options=options)
					
		return outclip

		
	def calc_serie_stats(self, table, stats_name, output_folder):
		"""
			Function to compute stats on temporal cloud and ndvi spectral table
			Ndvi stats : min max
			
			@param table: Spectral data, cloud raster and ndvi raster
			@type table: numpy.ndarray
			
			@returns: list of numpy.ndarray -- variable **account_stats**, list of temporal NDVI stats.
						  
						numpy.ndarray -- variable **account_cloud**, pixel number clear on the area.
		""" 

		ind = ['min({0})', 'max({0})']
		no_data_value = [99999, -10000]

		stack_ndvi = list(table[5])
		
		image = "im{0}b1"

		otb_MathBand = otb.Registry.CreateApplication("BandMathX")
		otb_MathBand.SetParameterStringList("il", stack_ndvi)

		path_stats = []

		for idx, i in enumerate(ind):

			expression = "({0} != {1} ? {0} : im1b1)".format(\
				i.format(",".join("({0}>-1000 ? {0} : {1})".format(\
					image.format(j+1), no_data_value[idx]) for j in range(len(stack_ndvi)))), no_data_value[idx])

			out_ndvistats_raster = "{0}/{1}.TIF".format(output_folder, stats_name[idx])

			path_stats.append(out_ndvistats_raster)

			otb_MathBand.SetParameterString("exp",expression)
			otb_MathBand.SetParameterString("out", out_ndvistats_raster)

			otb_MathBand.SetParameterOutputImagePixelType("out", otb.ImagePixelType_float)

			otb_MathBand.ExecuteAndWriteOutput()

		return path_stats

	
	def check_proj(self):
		"""
			Function to check if raster's projection is RFG93. 
			For the moment, PHYMOBAT works with one projection only Lambert 93 EPSG:2154 
		"""

		# Projection for PHYMOBAT

		proj_phymobat = 'AUTHORITY["EPSG","{0}"]'.format(Constantes.EPSG_PHYMOBAT)

		info_gdal = 'gdalinfo -json {0}'.format(self.image)
		info_json = json.loads(subprocess.check_output(info_gdal, shell=True))
		
		# Check if projection is in Lambert 93
		if not proj_phymobat in info_json['coordinateSystem']['wkt']:
			output_proj = "{0}_L93.tif".format(self.image[:-4])
			reproj = "gdalwarp -t_srs EPSG: {0} {1} {2}".format(Constantes.EPSG_PHYMOBAT, self.image, output_proj)
			os.system(reproj)
			# Remove old file and rename new file like the old file
			os.remove(self.image)
			os.rename(output_proj, self.image)
		