#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of PHYMOBAT 1.2.
# Copyright 2016 Sylvio Laventure (IRSTEA - UMR TETIS)
# 
# PHYMOBAT 1.2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PHYMOBAT 1.2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PHYMOBAT 1.2.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import math, subprocess

import osgeo.gdal
import otbApplication as otb

import Outils
import Satellites
import numpy as np

class RasterSat_by_date(object):
	"""
		Satellite image  processing's class. 
		This class include several processes to group images by date, mosaic images by date,
		extract images information, compute ndvi, compute cloud pourcent and create new rasters.
		
		@param class_archive: Archive class name with every information on downloaded images
		@type class_archive: class

		@param big_folder: Image processing folder
		@type big_folder: str

		@param one_date: [year, month, day] ...
				This variable is modified in the function :func:`mosaic_by_date()`. 
				To append mosaic image path, mosaic cloud image path, cloud pixel value table,
				 mosaic ndvi image path and ndvi pixel value table.
		@type one_date: list of str

		@param choice_nb_b: A option to choice output image number of band :
					func: `layer_rasterization` in Vector's class. 
					If this option is 0, it take input band. By default 0.
		@type choice_nb_b: int	
	"""  
	def __init__(self, class_archive, big_folder):
		"""
			Create a new 'Landsat_by_date' instance
		"""

		self.logger = Outils.Log("Log", "RasterSat_by_date")
		
		self._class_archive = class_archive
		self._big_folder = big_folder
		
		self.out_ds = None
		self.choice_nb_b = 0

	def group_by_date(self, d_uni):
		"""
			Function to extract images on a single date
			
			@param d_uni: [year, month, day]
			@type d_uni: list of str
			
			@returns: list of str -- variable **group** = [year, month, day, multispectral image path, cloud image path]
		"""
		 
		# Group of images with the same date
		group = []
		
		# Take images with the same date

		for d_dup in self._class_archive.list_img:
			if d_dup[:3] == d_uni:
				group.append(d_dup)
		 
		return group
	
	def vrt_translate_gdal(self, vrt_translate, src_data, dst_data):
		"""
			Function to launch gdal tools in command line. With ``gdalbuildvrt`` and ``gdal_translate``.
			This function is used :func:`mosaic_by_date` to mosaic image by date.
			
			@param vrt_translate: ``vrt`` or ``translate``
			@type vrt_translate: str
			
			@param src_data: Data source. Several data for vrt process and one data (vrt data) for gdal_translate
			@type src_data: list (process ``vrt``) or str (process ``translate``)  
			
			@param dst_data: Output path
			@type dst_data: str
		"""

		if os.path.exists(dst_data):
			os.remove(dst_data)
		
		# Select process
		if vrt_translate == 'vrt':
			
			# Verify input data
			if type(src_data) is not np.ndarray and type(src_data) is not list:
				self.logger.error('VRT file ! The data source should be composed of several data. A list minimal of 2 dimensions')
				sys.exit(1)
				
			self.logger.info('Build VRT file')
			if not os.path.exists(dst_data):
				options = osgeo.gdal.BuildVRTOptions(srcNodata=-10000)
				osgeo.gdal.BuildVRT(dst_data, src_data, options=options )
				
		elif vrt_translate == 'translate':
			# Verify input data
			try :
				src_data = str(src_data)
			except:
				self.logger.error('Geotiff file ! The data source should be composed of path file. A character string !')
				sys.exit(1)
				
			self.logger.info('Build Geotiff file')
			if not os.path.exists(dst_data):
				options = osgeo.gdal.TranslateOptions(noData=-10000)
				osgeo.gdal.Translate(dst_data, src_data, options=options)			
		
	def mosaic_by_date(self, date):
		"""
			Function to merge images of the same date in a image group : func:`group_by_date`.
		"""
		
		# Matrix multi images for a single date 
		group = self.group_by_date(date) # Every images [year, month, day, multispectral image, cloud image]
		group_ = np.transpose(np.array(group)) # Transpose matrix to extract path of images

		# Create a folder with images year if it doesn't exist
		mosaic_folder = "{0}/Mosaic".format(self._big_folder)
		
		if not os.path.exists(mosaic_folder):
			os.mkdir(mosaic_folder)

		# Create a folder with images date if it doesn't exist
		date_folder = "{0}/{1}".format(mosaic_folder, "".join(date))

		if not os.path.exists(date_folder):
			os.mkdir(date_folder)

		############################# Build VRT file with data images required #############################
		
		# Multispectral VRT outfile
		vrt_out = "{0}/{1}{2}.VRT".format(date_folder,self._class_archive._captor,os.path.basename(date_folder)) 

		if not os.path.exists(vrt_out):
			self.vrt_translate_gdal('vrt', list(group_[3]), vrt_out)

		# Cloud VRT outfile
		vrtcloud_out = "{0}/{1}{2}_NUA.VRT".format(date_folder,self._class_archive._captor,os.path.basename(date_folder)) 

		if not os.path.exists(vrtcloud_out):
			self.vrt_translate_gdal('vrt', list(group_[4]), vrtcloud_out)
		
		########################### Build Geotiff file with data images required ###########################
		
		# Multispectral TIF outfile
		gtif_out = "{0}.TIF".format(vrt_out[:-4])
		if not os.path.exists(gtif_out):
			self.vrt_translate_gdal('translate', vrt_out, gtif_out)
		
		# Cloud TIF outfile
		gtifcloud_out = "{0}.TIF".format(vrtcloud_out[:-4])
		if not os.path.exists(gtifcloud_out):
			self.vrt_translate_gdal('translate', vrtcloud_out, gtifcloud_out)

		self.cloudiness_pourcentage = [gtif_out, gtifcloud_out]
	
	def raster_data(self, img):
		"""
			Function to extract raster information.
			Return table of pixel values and raster information like
			 line number, pixel size, ... (gdal pointer)
			
			@param img 	: Raster path
			@type img  	: str

			@returns	: numpy.array -- variable **data**, Pixel value matrix of a raster.
					  
					  	  gdal pointer -- variable **_in_ds**, Raster information.
		"""
		
		# Load Gdal's drivers
		osgeo.gdal.AllRegister()

		self.logger.debug("Image : {0}".format(img))
		
		# Loading input raster
		self.logger.info('Loading input raster : {0}'.format(os.path.basename(img)))
		in_ds = osgeo.gdal.Open(img, osgeo.gdal.GA_ReadOnly)

		# if it doesn't exist
		if in_ds is None:
			self.logger.error('Could not open ')
			sys.exit(1)
		
		# Information on the input raster 
		if self.choice_nb_b == 0:
			nbband = in_ds.RasterCount # Spectral band number
		else:
			nbband = self.choice_nb_b

		rows = in_ds.RasterYSize # Rows number
		cols = in_ds.RasterXSize # Columns number
		
		# Table's declaration 
		data = [] 

		for band in range(nbband):
			
			canal = in_ds.GetRasterBand(band + 1) # Select a band
			
			if nbband == 1:
				# Assign pixel values at the data
				data = canal.ReadAsArray(0, 0, cols, rows).astype(np.float16) 
			else:
				data.append(canal.ReadAsArray(0, 0, cols, rows).astype(np.float16))
				
		###################################
		
		# Close input raster
		_in_ds = in_ds
		in_ds = None
		
		return data, _in_ds
	
	def pourc_cloud(self):
		"""
			Return clear pixel percentage on the image self.cloudiness_pourcentage[0]
			 because of a cloud image cloudiness_pourcentage[1].
					
			:returns: float -- variable **nb0**, clear pixel percentage.
			:Example:
			
			>>> import RasterSat_by_date
			>>> Landsat_test = RasterSat_by_date(class_archive, big_folder, one_date)
			>>> nb0_test = Landsat_test.pourc_cloud(Landsat_test._one_date[3], Landsat_test._one_date[4])
			>>> nb0_test
			98
		"""
		
		# Extract raster's information
		data_spec, info_spec = self.raster_data(self.cloudiness_pourcentage[0])
		data_cloud, info_cloud = self.raster_data(self.cloudiness_pourcentage[1])

		# Add cloud pixel value table
		# self.cloudiness_pourcentage.append(data_cloud) 
	
		# Extent of the images : 
		# ex : array([ True,  True,  True,  True, False,  True,  True,  True,  True], dtype=bool)
		# -> False where there is -10000 ou NaN
		mask_spec = np.isin(data_spec[0], [-10000, math.isnan], invert=True) 

		# Print area account of the pixel size 'info_spec.GetGeoTransform()'
		area = float((np.sum(mask_spec) * info_spec.GetGeoTransform()[1] * abs(info_spec.GetGeoTransform()[-1]) )/10000)
		self.logger.info("Area = {0} ha".format(area))

		############################## Cloud mask ##############################

		# This is the same opposite False where there is 0
		mask_cloud = np.isin(data_cloud, 0) 

		# If True in cloud mask, it take spectral image else False
		cloud = np.choose(mask_cloud, (False, mask_spec))

		# Sum of True. True is cloud 
		dist = np.sum(cloud) 

		# Computer cloud's percentage with dist (sum of cloud) by sum of the image's extent
		nb0 = float(dist)/(np.sum(mask_spec)) if np.sum(mask_spec) != 0 else 0 
		self.logger.info('For {0}, cloudy cover = {1}%'.format(os.path.basename(self.cloudiness_pourcentage[0]), 100 - round(nb0*100, 2)))
		
		return 1.0 - nb0
	
	def calcul_ndvi(self):
		"""
			Function to compute NDVI index for a Landsat image with OTB BandMathX
				- OTB help :
					* il : Images list : data and cloud
					* exp : Expression for ndvi : (PIR-R)/(PIR+R)
					* out : Feature Output Image 
		"""	

		canal_PIR = Satellites.SATELLITE[self._class_archive._captor]["PIR"]
		canal_R = Satellites.SATELLITE[self._class_archive._captor]["R"]

		img_ndvi = "{0}_ndvi.TIF".format(self.cloudiness_pourcentage[0][:-4])

		self.cloudiness_pourcentage.append(img_ndvi) # Add ndvi image path

		otb_MathBand = otb.Registry.CreateApplication("BandMathX")
		otb_MathBand.SetParameterStringList("il", [self.cloudiness_pourcentage[0], self.cloudiness_pourcentage[1]])
		otb_MathBand.SetParameterString("exp",\
		"(im1b1 > -1 ? (im2b1 == 0  ? ndvi(im1b{1}, im1b{0}) : -1) : im1b1)"\
		.format(canal_PIR, canal_R))
		otb_MathBand.SetParameterString("out", img_ndvi)

		otb_MathBand.ExecuteAndWriteOutput()