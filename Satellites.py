#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections

SATELLITE = collections.defaultdict(dict)

	# Info from Theia-land website or Olivier Hagolle blog

########################## SENTINEL2 ###############################

SATELLITE["SENTINEL2"]["server"] = "https://theia.cnes.fr/atdistrib"
SATELLITE["SENTINEL2"]["resto"] = "resto2"
SATELLITE["SENTINEL2"]["token_type"] = "text"
SATELLITE["SENTINEL2"]["R"] = 2
SATELLITE["SENTINEL2"]["PIR"] = 3

########################## LANDSAT #################################

SATELLITE["Landsat"]["server"] = "https://theia-landsat.cnes.fr"
SATELLITE["Landsat"]["resto"] = "resto"
SATELLITE["Landsat"]["token_type"] = "json"
SATELLITE["Landsat"]["R"] = 3
SATELLITE["Landsat"]["PIR"] = 4




