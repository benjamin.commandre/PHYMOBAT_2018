#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

NIVEAU_DEFAUT = logging.DEBUG

SIMPLE_MODE = 0
EXPERT_MODE = 1

MULTIPROCESSING_ENABLE = True
MULTIPROCESSING_DISABLE = False

CLOUD_THRESHOLD = 0.4

EPSG_PHYMOBAT = 2154
